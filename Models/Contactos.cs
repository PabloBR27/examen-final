﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinal.Models
{
    public class Contactos
    {
        public int Id { get; set; }
        public int IdPrimeraCuenta { get; set; }//el solicitante
        public int IdSegundaCuenta { get; set; }//el usuario logeado
        public string CorreoPCuenta { get; set; }//el solicitante
        public string CorreoSCuenta { get; set; }//el usuario logeado
    }
}
