﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinal.Models
{
    public class SolicitudAmistad
    {
        public int Id { get; set; }
        public int IdUserSolicitante { get; set; }
        public int IdUserDestino { get; set; }
        public string CorreoOrigen { get; set; }
        //public string Nombre { get; set; }
    }
}
