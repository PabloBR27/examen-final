﻿
namespace ExamenFinal.Models
{
    public class Cuenta
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public string Nombre { get; set; }
        public string Categoria { get; set; }
        public double Saldo { get; set; }
    }
}
