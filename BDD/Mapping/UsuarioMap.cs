﻿using Microsoft.EntityFrameworkCore;
using ExamenFinal.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExamenFinal.BDD.Mapping
{
    public class UsuarioMap : IEntityTypeConfiguration<Usuario>
    {
        public void Configure(EntityTypeBuilder<Usuario> builder)
        {
            builder.ToTable("Usuario");
            builder.HasKey(u => u.Id);
        }
    }
}
