﻿using Microsoft.EntityFrameworkCore;
using ExamenFinal.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExamenFinal.BDD.Mapping
{
    public class GastoMap : IEntityTypeConfiguration<Gasto>
    {
        public void Configure(EntityTypeBuilder<Gasto> builder)
        {
            builder.ToTable("Gasto");
            builder.HasKey(g => g.Id);
        }
    }
}
