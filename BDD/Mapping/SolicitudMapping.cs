﻿using Microsoft.EntityFrameworkCore;
using ExamenFinal.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExamenFinal.BDD.Mapping
{
    public class SolicitudMapping : IEntityTypeConfiguration<SolicitudAmistad>
    {
        public void Configure(EntityTypeBuilder<SolicitudAmistad> builder)
        {
            builder.ToTable("SolicitudAmistad");
            builder.HasKey(s=>s.Id);
        }
    }
}
