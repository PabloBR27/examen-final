﻿using Microsoft.EntityFrameworkCore;
using ExamenFinal.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
namespace ExamenFinal.BDD.Mapping
{
    public class CuentaMap : IEntityTypeConfiguration<Cuenta>
    {
        public void Configure(EntityTypeBuilder<Cuenta> builder)
        {
            builder.ToTable("Cuenta");
            builder.HasKey(c => c.Id);
        }
    }
}
