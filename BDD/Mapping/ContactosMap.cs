﻿using Microsoft.EntityFrameworkCore;
using ExamenFinal.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExamenFinal.BDD.Mapping
{
    public class ContactosMap : IEntityTypeConfiguration<Contactos>
    {
        public void Configure(EntityTypeBuilder<Contactos> builder)
        {
            builder.ToTable("Contactos");
            builder.HasKey(c => c.Id);
        }
    }
}
