﻿using Microsoft.EntityFrameworkCore;
using ExamenFinal.Models;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ExamenFinal.BDD.Mapping
{
    public class IngresoMap :IEntityTypeConfiguration<Ingreso>
    {
        public void Configure(EntityTypeBuilder<Ingreso> builder)
        {
            builder.ToTable("Ingreso");
            builder.HasKey(g => g.Id);
        }
    }
}
