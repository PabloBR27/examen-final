﻿using Microsoft.EntityFrameworkCore;
using ExamenFinal.Models;
using ExamenFinal.BDD.Mapping;

namespace ExamenFinal.BDD
{
    public class FinanzasContext: DbContext
    {
        public DbSet<Cuenta> Cuentas { get; set; }
        public DbSet<Gasto> Gastos { get; set; }
        public DbSet<Ingreso> Ingresos { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<SolicitudAmistad> Solicitudes { get; set; }
        public DbSet<Contactos> Contactos { get; set; }

        public FinanzasContext(DbContextOptions<FinanzasContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new IngresoMap());
            modelBuilder.ApplyConfiguration(new CuentaMap());
            modelBuilder.ApplyConfiguration(new GastoMap());
            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new SolicitudMapping());
            modelBuilder.ApplyConfiguration(new ContactosMap());

        }
    }
}
