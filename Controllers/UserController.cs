﻿using ExamenFinal.BDD;
using ExamenFinal.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ExamenFinal.Controllers
{

    [Authorize]
    [Route("usuarios")]
    public class UserController : Controller
    {
        private FinanzasContext _context;
        
        public UserController(FinanzasContext _context)
        {
            this._context = _context;
        }
        public IActionResult Index()
        {
            return View();
        }
        [Authorize]
        [Route("usuarios-disponibles")]
        public IActionResult Usuarios()
        {
            var lusuarios = _context.Usuarios;
            List<Usuario> usuarios = lusuarios.ToList();
            //lusuarios.Remove(GetLoggedUser());
            usuarios.Remove(GetLoggedUser());
            
            return View("Usuarios", usuarios);
        }
        //muestra las solicitudes de amistad
        [Route("solicitudes-amistad")]
        public IActionResult GetSolicitudesAUsuario()
        {
            var Solicitudes = _context.Solicitudes.Where(u => u.IdUserDestino == GetLoggedUser().Id);
            return View("GetSolicitudesAUsuario", Solicitudes);
        }

        //Realiza la solicitud
        [Route("solicita-post")]
        public IActionResult SolicitaAmistad(int idSol,string correo)
        {
            var usuarioSolicitado = _context.Usuarios.FirstOrDefault(u => u.Id == idSol);
            SolicitudAmistad solicitud = new SolicitudAmistad()
            {
                IdUserSolicitante = GetLoggedUser().Id,
                IdUserDestino = idSol,
                CorreoOrigen = GetLoggedUser().Correo
            };

            _context.Solicitudes.Add(solicitud);
            _context.SaveChanges();

            //return View();
            return RedirectToAction("GetContactosUser");
        }
        //acepta la solicitud
        [Route("aceptar-sol")]
        public IActionResult AceptaSolicitud(int idS)
        {
            var solicitud = _context.Solicitudes.FirstOrDefault(s => s.Id == idS);
            Contactos newContact = new Contactos()
            {
                //cuenta del solicitante
                IdPrimeraCuenta = solicitud.IdUserSolicitante,
                CorreoPCuenta = solicitud.CorreoOrigen,
                //cuenta del usuario que acepta (el que está conectado)
                IdSegundaCuenta = solicitud.IdUserDestino,
                CorreoSCuenta = GetLoggedUser().Correo
            };
            _context.Solicitudes.Remove(solicitud);
            _context.Contactos.Add(newContact);
            _context.SaveChanges();
            return RedirectToAction("GetContactosUser");
        }
        //contactos del usuario
        [Route("contactos")]
        public IActionResult GetContactosUser()
        {
            var contactos = _context.Contactos.Where(u => u.IdSegundaCuenta == GetLoggedUser().Id);
            return View("GetContactosUser",contactos);
        }
        private Usuario GetLoggedUser()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = _context.Usuarios.First(u => u.Correo == username);
            return user;
        }
    }
}
