﻿using Microsoft.AspNetCore.Mvc;
using ExamenFinal.BDD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ExamenFinal.Models;
using Microsoft.AspNetCore.Authorization;

namespace Toma_nota.Controllers
{
    public class InfoPantallaCuentaTransferencia
    {
        public  List<Cuenta> ListaCuentasUsuario { get; set; }
        public List<Cuenta> ListaCuentasDestino { get; set; }
        public Usuario UserDestino { get; set; }
        //public Cuenta CuentaDestino { get; set; }
    }
    public class Transferencia
    {
        public int IdCuentaDestino { get; set; }
        public string CuentaOrigen { get; set; }
        public string CuentaDestino { get; set; }
        public string Descripcion { get; set; }
        public int Monto { get; set; }
    }
    [Authorize]
    public class FinanzasController : Controller
    {
        private FinanzasContext _context;
        public FinanzasController(FinanzasContext context)
        {
            _context = context;
        }

        public IActionResult Finanzas()
        {
            var cuentas = _context.Cuentas.Where(c=>c.IdUsuario == GetLoggedUser().Id);
            return View("Finanzas",cuentas);
        }
        public IActionResult CrearCuenta()
        {
            return View();
        }
        [HttpPost]
        public IActionResult CrearCuenta(Cuenta cuenta)
        {
            cuenta.IdUsuario = GetLoggedUser().Id;
            _context.Cuentas.Add(cuenta);
            _context.SaveChanges();
            return RedirectToAction("CrearCuenta");
        }
        public IActionResult MuestraGastos()
        {
            var lgastos = _context.Gastos;
            var cuentas = _context.Cuentas.Where(c => c.IdUsuario == GetLoggedUser().Id);
            List<Gasto> gastos = new List<Gasto>();
            //var lIngresos = _context.Ingresos.Where(i => i.Cuenta == c.Nombre).ToList();
            foreach (Cuenta c in cuentas)
            {
                foreach (Gasto gasto in lgastos)
                {
                    if (gasto.Cuenta == c.Nombre)
                        gastos.Add(gasto);
                }
            }
            return View("MuestraGastos", gastos);
        }
        [HttpGet]
        public IActionResult RegistraGasto()
        {
            List<Cuenta> Cuentas = GetCuentaUser().Where(u => u.IdUsuario == GetLoggedUser().Id).ToList();
            return View("RegistraGasto", Cuentas);
        }
        [HttpPost]
        public IActionResult RegistraGasto(Gasto gasto)
        {

            Cuenta cuenta = GetCuentaUser().FirstOrDefault(c => c.Nombre == gasto.Cuenta);
            if(gasto.Monto > cuenta.Saldo)
            {
                ModelState.AddModelError("El monto no puede ser mayor al saldo de la cuenta. Saldo de la cuenta",cuenta.Saldo.ToString());
                return View();
            }
            cuenta.Saldo = calculaSaldoTotal(gasto.Monto, gasto.Cuenta, 1);
            //_context.cuentas.Update(cuenta);

            gasto.FechaRegistro = new DateTime();
            gasto.IdCuenta = GetLoggedUser().Id;
            _context.Gastos.Add(gasto);
            _context.SaveChanges();
            return RedirectToAction("Finanzas");
        }
        [HttpGet]
        public IActionResult MuestraIngresos()
        {
            var cuentas = _context.Cuentas.Where(c => c.IdUsuario == GetLoggedUser().Id);
            var lingresos = _context.Ingresos;
            List<Ingreso> ingresos = new List<Ingreso>();
            //var lIngresos = _context.Ingresos.Where(i => i.Cuenta == c.Nombre).ToList();
            foreach (Cuenta c in cuentas)
            {
                foreach (Ingreso ingreso in lingresos)
                {
                    if (ingreso.Cuenta == c.Nombre)
                        ingresos.Add(ingreso);
                }
            }
            //var ingresos = _context.Ingresos.Where(i=>i.)
            
            return View("MuestraIngresos", ingresos);
        }
        //ingresos
        [HttpGet]
        public IActionResult CreaIngreso()
        {
            List<Cuenta> Cuentas = GetCuentaUser().Where(u => u.IdUsuario == GetLoggedUser().Id).ToList();
            return View("CreaIngreso",Cuentas);
        }
        [HttpPost]
        public IActionResult CreaIngreso(Ingreso ingreso)
        {
            ingreso.FechaIngreso = new DateTime();
            ingreso.IdCuenta = GetLoggedUser().Id;
            var cuenta = _context.Cuentas.FirstOrDefault(c => c.Nombre == ingreso.Cuenta);
            
            calculaSaldoTotal(ingreso.Monto,cuenta.Nombre,2);

            _context.Ingresos.Add(ingreso);
            _context.SaveChanges();
            return RedirectToAction("Finanzas");
        }
        //Transferencia tarjeta-tarjeta
        public IActionResult Transferencia()
        {
            //var cuentas = _context.Cuentas.Where(c => c.IdUsuario == GetLoggedUser().Id);
            InfoPantallaCuentaTransferencia pantallaTransferencia = new InfoPantallaCuentaTransferencia()
            {
                ListaCuentasUsuario = GetCuentaUser().Where(u => u.IdUsuario == GetLoggedUser().Id).ToList(),
                UserDestino = _context.Usuarios.FirstOrDefault(u => u.Id == GetLoggedUser().Id)
                //CuentaDestino = _context.Cuentas.FirstOrDefault(c => c.Id == idC)
            };
            return View("Transferencia", pantallaTransferencia);
        }
        public IActionResult TransferenciaPost(Transferencia trans)
        {
            var cuentaOrigen = _context.Cuentas.FirstOrDefault(c => c.IdUsuario == GetLoggedUser().Id && c.Nombre == trans.CuentaOrigen);
            var cuentaDestino = _context.Cuentas.FirstOrDefault(c => c.IdUsuario == GetLoggedUser().Id && c.Nombre == trans.CuentaDestino);
            trans.Descripcion = "Transferencia";
            DateTime fech = DateTime.Now;
            Ingreso ingreso = new Ingreso
            {
                IdCuenta = GetLoggedUser().Id,
                Cuenta = trans.CuentaDestino,
                Descripcion = trans.Descripcion,
                FechaIngreso = fech,
                Monto = trans.Monto
            };

            Gasto gasto = new Gasto
            {
                IdCuenta = GetLoggedUser().Id,
                Cuenta = trans.CuentaOrigen,
                FechaRegistro = fech,
                Monto = trans.Monto,
                Descripcion = trans.Descripcion
            };

            calculaSaldoTotal(trans.Monto, cuentaOrigen.Nombre, 1);//gasto
            calculaSaldoTotal(trans.Monto,cuentaDestino.Nombre , 2);//ingreso

            _context.Ingresos.Add(ingreso);
            _context.Gastos.Add(gasto);
            _context.SaveChanges();


            return RedirectToAction("Finanzas");
        }


        //acciones del usuario con sus contactos
        public IActionResult TransferenciaUsuario(int idC)//id del usuario
        {
            var cuenta = _context.Cuentas.FirstOrDefault(c => c.IdUsuario == idC);
            InfoPantallaCuentaTransferencia pantallaTransferencia = new InfoPantallaCuentaTransferencia()
            {
                ListaCuentasUsuario = GetCuentaUser().Where(u => u.IdUsuario == GetLoggedUser().Id).ToList(),
                ListaCuentasDestino = GetCuentaUser().Where(u => u.IdUsuario == idC).ToList(),
                UserDestino = _context.Usuarios.FirstOrDefault(u => u.Id == idC)
                //CuentaDestino = _context.Cuentas.FirstOrDefault(c => c.Id == idC)
            };

            return View("TransferenciaUsuario", pantallaTransferencia);
        }
        //guarda la transferencia a un contacto
        public IActionResult TransfiereDineroUsuario(Transferencia trans)
        {
            //ccrear un objeto de ingreso y uno de gasto
            //pasar el objeto ingreso a la funcion 
            //var cuenta = _context.Cuentas.FirstOrDefault(c =>c.Nombre == trans.CuentaOrigen && c.IdUsuario == trans.IdCuentaDestino);
            //obtiene la cuenta de origen

            DateTime fech = DateTime.Now;
            

            Ingreso ingreso = new Ingreso
            {
                IdCuenta = _context.Cuentas.FirstOrDefault(c => c.Nombre == trans.CuentaDestino).Id,
                Cuenta = trans.CuentaDestino,
                Descripcion = trans.Descripcion,
                FechaIngreso = fech,
                Monto = trans.Monto
            };

            Gasto gasto = new Gasto
            {
                IdCuenta = _context.Cuentas.FirstOrDefault(c => c.Nombre == trans.CuentaOrigen).Id,
                Cuenta = GetCuentaUser().FirstOrDefault(c => c.Nombre == trans.CuentaOrigen).Nombre,
                FechaRegistro = fech,
                Monto = trans.Monto,
                Descripcion = trans.Descripcion
            };

            var cuentaOr = _context.Cuentas.FirstOrDefault(c => c.IdUsuario == GetLoggedUser().Id);
            Cuenta cuentaUserOrigen = _context.Cuentas.FirstOrDefault(c=>c.Nombre == trans.CuentaOrigen && c.IdUsuario == GetLoggedUser().Id);
            //_context.Cuentas.Update(cuentaUserOrigen);
            //id del usuario
            Cuenta cuentaUserDestino = _context.Cuentas.FirstOrDefault(c => c.Nombre == trans.CuentaDestino && c.IdUsuario == trans.IdCuentaDestino);
            

            calculaSaldoTotal(trans.Monto,cuentaUserOrigen.Nombre,1);
            calculaSaldoTotal(trans.Monto, cuentaUserDestino.Nombre, 2);


            _context.Ingresos.Add(ingreso);
            _context.Gastos.Add(gasto);
            _context.SaveChanges();


            return RedirectToAction("Finanzas");
        }

        public double calculaSaldoTotal(double monto, string cuentaUser, byte tipo)
        {
            var cuentaUsuario = _context.Cuentas.FirstOrDefault(c => c.Nombre == cuentaUser);

            Cuenta cuenta = cuentaUsuario;
            Cuenta temp = cuenta;

            if (tipo == 1)//Gasto
            {
                cuenta.Saldo -= monto;
            }
            else if (tipo == 2)//ingreso
            {
                cuenta.Saldo += monto;
            }
            cuentaUsuario = cuenta;
            _context.Cuentas.Update(cuentaUsuario);
            _context.SaveChanges();

            return cuenta.Saldo;
        }
        public double saldoCuenta(int id)
        {
            Cuenta count_user = GetCuentaUser().FirstOrDefault(c => c.Id == id);

            return count_user.Saldo;
        }
        //devuelve todas las cuentas
        public List<Cuenta> GetCuentaUser()
        {
            return _context.Cuentas.ToList();
        }
        private Usuario GetLoggedUser()
        {
            var claim = HttpContext.User.Claims.First();
            string username = claim.Value;
            var user = _context.Usuarios.First(u=>u.Correo== username);
            return user;
        }
    }
}
