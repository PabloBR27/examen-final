﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Configuration;
using System.Security.Cryptography;
using System.Security.Claims;
using ExamenFinal.BDD;
using ExamenFinal.Models;
using System.Text;

namespace ExamenFinal.Controllers
{
    [Route("auth")]
    public class AuthController : Controller
    {
        private FinanzasContext context;

        private IConfiguration configuration;

        public AuthController(FinanzasContext context, IConfiguration config)
        {
            this.context = context;
            this.configuration = config;
        }
        /**/
        [HttpGet]
        [Route("login")]
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        [Route("login-post")]
        public IActionResult Login(string correo, string password)
        {
            var user = context.Usuarios
                .FirstOrDefault(u=>u.Correo == correo && u.Contrasenia == CreateHash(password));
            if (user == null)
            {
                TempData["AuthMessage"] = "Usuario o contraseña incorrecto";
                return RedirectToAction("Login");
            }
            var claims = new List<Claim>
            {
                 new Claim(ClaimTypes.Name, user.Correo),
            };

            var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            HttpContext.SignInAsync(claimsPrincipal);
 
            return RedirectToAction("Finanzas", "Finanzas");
        }
        [Route("logout")]
        public IActionResult Logout()
        {
            HttpContext.SignOutAsync();
            return RedirectToAction("Login");
        }

        /**/
        public string Create(string password)
        {
            return CreateHash(password);
        }

        private string CreateHash(string input)
        {
            input += configuration.GetValue<string>("Key");
            var sha = SHA512.Create();

            var bytes = Encoding.Default.GetBytes(input);
            var hash = sha.ComputeHash(bytes);

            return Convert.ToBase64String(hash);
        }
        /**/
        [HttpGet]
        [Route("create-account")]
        public IActionResult CrearCuenta()
        {
            return View();
        }
        [HttpPost]
        [Route("create-account-post")]
        public IActionResult CrearCuenta(Usuario userReg)
        {
            string a = userReg.Contrasenia;
            Console.Write(a);
            string pass = CreateHash(userReg.Contrasenia);
            // no hay mensajes => 0 mensaje
            Usuario user = new Usuario
            {
                Correo = userReg.Correo,
                Contrasenia = pass,
                NumeroCelular = userReg.NumeroCelular,
                Nombre = userReg.Nombre,
                Apellido = userReg.Apellido,
                DNI = userReg.DNI
            };

            context.Usuarios.Add(user);
            context.SaveChanges();
            return RedirectToAction("login");
        }

    public IActionResult Index()
        {
            return View();
        }
    }
}
